package my.domain.photogallery.PhotoGallery;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;

import my.domain.photogallery.PhotoPage.PhotoPageActivity;
import my.domain.photogallery.PollService;
import my.domain.photogallery.R;

/**
 * Created by Admin on 23/05/2017.
 */

public class PhotoGalleryFragment extends Fragment implements PhotoGalleryView{
    private RecyclerView mRecyclerView;
    public PhotoGalleryPresenterImpl mPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
        mPresenter = new PhotoGalleryPresenterImpl(this);
        mPresenter.fetchItems();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_photo_gallery, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.recyclerView_fragment_photo_gallery);

        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);

        mRecyclerView.setAdapter(new PhotoGridAdapter());

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_photo_gallery, menu);

        MenuItem searchItem = menu.findItem(R.id.menu_item_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        SearchManager searchManager = (SearchManager)getActivity().getSystemService(Context.SEARCH_SERVICE);
        ComponentName name = getActivity().getComponentName();
        SearchableInfo searchInfo = searchManager.getSearchableInfo(name);

        searchView.setSearchableInfo(searchInfo);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        MenuItem toggleItem = menu.findItem(R.id.menu_item_toggle_polling);
        if(PollService.isServiceAlarmOn(getActivity())){
            toggleItem.setTitle(R.string.stop_polling);
        }else{
            toggleItem.setTitle(R.string.start_polling);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mPresenter.onOptionsMenuSelected(getActivity(), item);
        switch (item.getItemId()){
            case R.id.menu_item_search:
                getActivity().onSearchRequested();
                return true;
            case R.id.menu_item_clear:
                return true;
            case R.id.menu_item_toggle_polling:
                getActivity().invalidateOptionsMenu();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void photosFetched() {
        mRecyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void photoThumbnailDownloaded(ImageView imageView, Bitmap thumbnail) {
        ((PhotoGalleryFragment.PhotoGridAdapter) mRecyclerView.getAdapter()).onThumbnailDownloaded(imageView, thumbnail);
    }

    @Override
    public void goToPhotoPage(String url) {
        Uri photoPageUri = Uri.parse(url);
        Intent i = new Intent(getActivity(), PhotoPageActivity.class);
        i.setData(photoPageUri);

        startActivity(i);
    }

    private class PhotoGridAdapter extends RecyclerView.Adapter<PhotoGridViewHolder>{
        @Override
        public PhotoGridViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item_photo_gallery, parent, false);

            return new PhotoGridViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final PhotoGridViewHolder holder, int position) {
            holder.mImageViewPhoto.setImageResource(R.mipmap.brian_up_close);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPresenter.listItemPressed(holder.getAdapterPosition());
                }
            });
            mPresenter.fetchThumbnail(holder.mImageViewPhoto, position);
        }

        @Override
        public int getItemCount() {
            return mPresenter.getItemCount();
        }

        void onThumbnailDownloaded(ImageView imageView, Bitmap thumbnail) {
            if (isVisible()) {
                imageView.setImageBitmap(thumbnail);
            }
        }
    }

    class PhotoGridViewHolder extends RecyclerView.ViewHolder {
        private ImageView mImageViewPhoto;

        PhotoGridViewHolder(View itemView) {
            super(itemView);
            mImageViewPhoto = (ImageView) itemView.findViewById(R.id.imageView_grid_item);
        }
    }
}
