package my.domain.photogallery.PhotoGallery;

import android.app.Activity;
import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * Created by Admin on 29/05/2017.
 */

interface PhotoGalleryView {
    Activity getActivity();

    void photosFetched();
    void photoThumbnailDownloaded(ImageView imageView, Bitmap thumbnail);

    void goToPhotoPage(String url);
}
