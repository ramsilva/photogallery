package my.domain.photogallery.PhotoGallery;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.widget.ImageView;

import java.util.ArrayList;

import my.domain.photogallery.Fetching.FlickrFetchr;
import my.domain.photogallery.PollService;
import my.domain.photogallery.R;
import my.domain.photogallery.Fetching.ThumbnailDownloader;

/**
 * Created by Admin on 29/05/2017.
 */

public class PhotoGalleryPresenterImpl implements PhotoGalleryPresenter<ImageView>, ThumbnailDownloader.Listener<ImageView> {
    private PhotoGalleryView mGalleryView;
    private ThumbnailDownloader<ImageView> mThumbnailThread;
    private ArrayList<GalleryItem> mPhotos;

    PhotoGalleryPresenterImpl(PhotoGalleryView galleryView) {
        mGalleryView = galleryView;

        mThumbnailThread = new ThumbnailDownloader<>(new Handler());
        mThumbnailThread.start();
        mThumbnailThread.getLooper();
        mThumbnailThread.setListener(this);
    }

    @Override
    public void fetchItems() {
        new FetchItemsTask().execute();
    }

    @Override
    public void fetchThumbnail(ImageView imageView, int position) {
        GalleryItem item = mPhotos.get(position);
        mThumbnailThread.queueThumbnail(imageView, item.getUrl());
    }

    @Override
    public int getItemCount() {
        return mPhotos == null ? 0 : mPhotos.size();
    }

    @Override
    public void onDestroyView() {
        mThumbnailThread.clearQueue();
    }

    @Override
    public void onDestroy() {
        mThumbnailThread.quit();
    }

    @Override
    public void onOptionsMenuSelected(Activity activity, MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_item_clear:
                PreferenceManager.getDefaultSharedPreferences(activity)
                        .edit()
                        .putString(FlickrFetchr.PREF_SEARCH_QUERY, null)
                        .apply();
                fetchItems();
            case R.id.menu_item_toggle_polling:
                boolean shouldStartAlarm = !PollService.isServiceAlarmOn(activity);
                PollService.setServiceAlarm(activity, shouldStartAlarm);
        }
    }

    @Override
    public void listItemPressed(int position) {
        mGalleryView.goToPhotoPage(mPhotos.get(position).getPhotoPageUrl());
    }

    @Override
    public void onThumbnailDownloaded(ImageView imageView, Bitmap thumbnail) {
        mGalleryView.photoThumbnailDownloaded(imageView, thumbnail);
    }

    private class FetchItemsTask extends AsyncTask<Void, Void, ArrayList<GalleryItem>> {
        @Override
        protected ArrayList<GalleryItem> doInBackground(Void... params) {
            Activity activity = mGalleryView.getActivity();
            if(activity == null) return new ArrayList<>();

            String query = PreferenceManager.getDefaultSharedPreferences(activity)
                    .getString(FlickrFetchr.PREF_SEARCH_QUERY, null);
            if(query == null){
                return new FlickrFetchr().fetchItems();
            }else{
                return new FlickrFetchr().search(query);
            }
        }

        @Override
        protected void onPostExecute(ArrayList<GalleryItem> items) {
            mPhotos = items;
            mGalleryView.photosFetched();
        }
    }
}
