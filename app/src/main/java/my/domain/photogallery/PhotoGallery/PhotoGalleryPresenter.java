package my.domain.photogallery.PhotoGallery;

import android.app.Activity;
import android.view.MenuItem;

/**
 * Created by Admin on 29/05/2017.
 */

public interface PhotoGalleryPresenter<Token> {
    void onDestroyView();
    void onDestroy();
    void onOptionsMenuSelected(Activity activity, MenuItem item);

    void listItemPressed(int position);

    void fetchItems();
    void fetchThumbnail(Token token, int position);
    int getItemCount();
}
