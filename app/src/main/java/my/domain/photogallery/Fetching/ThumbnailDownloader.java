package my.domain.photogallery.Fetching;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;
import android.util.LruCache;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;

/**
 * Created by Admin on 23/05/2017.
 */

public class ThumbnailDownloader<Token> extends HandlerThread {
    private static final int MESSAGE_DOWNLOAD = 0;

    Handler mHandler;
    Map<Token, String> mRequestMap = Collections.synchronizedMap(new HashMap<Token, String>());
    LruCache<String, Bitmap> mThumbnailCache = new LruCache<>(4 * 1024 * 1024);
    Handler mResponseHandler;
    Listener<Token> mListener;

    public ThumbnailDownloader(Handler responseHandler) {
        super(TAG);
        mResponseHandler = responseHandler;
    }

    @Override
    protected void onLooperPrepared() {
        mHandler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                if(msg.what == MESSAGE_DOWNLOAD){
                    Token token = (Token)msg.obj;
                    Log.i(TAG, "Got a request for url: " + mRequestMap.get(token));
                    handleRequest(token);
                }
            }
        };
    }

    private void handleRequest(final Token token) {
        try{
            final String url = mRequestMap.get(token);
            if(url == null) return;

            final Bitmap bitmap;
            if(mThumbnailCache.get(url) != null){
                Log.i(TAG, "Bitmap was cached");
                bitmap = mThumbnailCache.get(url);
            }else{
                Log.i(TAG, "Bitmap was downloaded");
                byte[] bitmapBytes = new FlickrFetchr().getUrlBytes(url);
                bitmap = BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length);
                mThumbnailCache.put(url, bitmap);
            }

            mResponseHandler.post(new Runnable() {
                @Override
                public void run() {
                    if(mRequestMap.get(token) != url) return;

                    mRequestMap.remove(token);
                    mListener.onThumbnailDownloaded(token, bitmap);
                }
            });
        } catch (IOException e) {
            Log.e(TAG, "Error downloading image", e);
        }
    }

    public void clearQueue(){
        mHandler.removeMessages(MESSAGE_DOWNLOAD);
        mRequestMap.clear();
    }

    public void queueThumbnail(Token token, String url){
        Log.i(TAG, "Got an url: " + url);
        mRequestMap.put(token, url);
        mHandler.obtainMessage(MESSAGE_DOWNLOAD, token).sendToTarget();
    }

    public interface Listener<Token>{
        void onThumbnailDownloaded(Token token, Bitmap thumbnail);
    }

    public void setListener(Listener<Token> listener) {
        mListener = listener;
    }
}
